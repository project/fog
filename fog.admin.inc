<?php

/**
 * @file
 *  fog.admin.inc
 * The module admin  system, which controls the config of fastopengraph module.
 */
function fog_admin_settings() {
    $form = array();
    global $conf;

    $form['fog'] = array(
        '#type' => 'fieldset',
    );
    $form['fog']['fog_options'] = array(
        '#type' => 'checkboxes',
        '#default_value' => isset($conf['fog']) ? $conf['fog']['fog_options'] : array(),
        '#options' => array(
            'use_avatar' => t('Use node author avatar for node images as additional alternative.'),
            'use_logo' => t('Use website logo for node images as additional alternative.'),
        ),
    );
    $form['fog']['default_description'] = array(
        '#type' => 'textarea',
        '#default_value' => isset($conf['fog']) ? $conf['fog']['default_description'] : $conf['site_name'],
        '#description' => t('Enter default description for nodes without body and teaser.
          It will be displayed on external resources like Facebook. Limit 300 chars. No HTML.'),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save OpenGraph settings'),
    );

    return $form;
}

function fog_admin_settings_submit($form, &$form_state) {

    $fog = array();
    global $conf;

    $fog['fog_options'] = $form_state['values']['fog_options'];
    $fog['default_description'] = !empty($form_state['values']['default_description']) ? mb_substr(strip_tags($form_state['values']['default_description']), 0, 300) : $conf['site_name'];

    variable_set('fog', $fog);
}